//
//  CSJUserAccountViewModel.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/11.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

class CSJUserAccountViewModel: NSObject {

    static let shardUserAccount: CSJUserAccountViewModel = CSJUserAccountViewModel()
    
    //用户账号模型
    var userAccount: CSJUserAccount? {
    
    return CSJUserAccount.readUserAccount()
    }
    //判断是否登录
    var islogin: Bool {
    
    return accessToken != nil
    }
    //封装accesstoken,判断逻辑
    var accessToken: String? {
        
        guard let token = userAccount?.access_token else {
 
            return nil
        }
    
//      let result = userAccount?.expiresDate?.compare(NSDate())
//        if result == NSComparisonResult.OrderedAscending {
//        
//        return token
//        
//        }else {
//        
//        return nil
//        }
        
        //  如果accesstoken不为nil,判断accesstoken的时间是否过期
        let result =  userAccount?.expiresDate?.compare(NSDate())
        //  如果是降序表示accesstoken没有过期
        if result == NSComparisonResult.OrderedDescending {
            return token
        } else {
            return nil
        }
    }
    
    
    func getAccessToken(code: String, callBack:(isSuccess: Bool) -> ()) {
        
        
        CSJNetTools.sharedTools.getAccessToken(code) { (response, error) -> () in
            
            if error != nil {
                
                print("有误")
                callBack(isSuccess: false)
                return
                
            }
            guard let dict = response as? [String: AnyObject]else {
                callBack(isSuccess: false)
                return
            }
            
            let userAccount = CSJUserAccount(dict: dict)
            
            self.userMessage(userAccount,callBack: callBack)
            
        }

    }
    
    func userMessage(userAccount: CSJUserAccount, callBack:(isSuccess: Bool) -> ()) {
        
        CSJNetTools.sharedTools.userMessage(userAccount) { (response, error) -> () in
            
            if error != nil {
                
                print("失败")
                 callBack(isSuccess: false)
                return
            }
            guard let dit = response as? [String: AnyObject] else {
                 callBack(isSuccess: false)
                return
            }
            
            // let userMage = CSJUserAccount(dict: dit)
            //
            // let name = userMage.name
            // let avatar_large = userMage.avatar_large
            //
            
            let name1 = dit["name"]
            let avatar_large1 = dit["avatar_large"]
            
            userAccount.name = name1 as? String
            userAccount.avatar_large = avatar_large1 as? String
            
            print(userAccount.name, userAccount.avatar_large)
                        
            print("-------\(userAccount)")
            
            userAccount.saveUserAccount()
            callBack(isSuccess: true)
        }
    }
        
        
}
