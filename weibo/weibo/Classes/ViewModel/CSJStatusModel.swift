//
//  CSJStatusModel.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/13.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SDWebImage
class CSJStatusModel: NSObject {
    //转发数
    var retweetCount: String?
    //评论数
    var commentCount: String?
    //赞数
    var unlikeCount: String?
    
    var status: StatusModel?
    //  转发微博内容
    var retweetContent: String?
    //  来源数据
    var sourceContent: String?
    //  会员等级图片
    var mbrankImage: UIImage?
    //  认证类型等级图片
    var verifiedTypeImage: UIImage?
    
    
    var a: CSJStatusViewModel = CSJStatusViewModel()
    
    init(status: StatusModel) {
        super.init()
        
        self.status = status
        
        retweetCount = handleCount(status.reposts_count, title: "转发")
        commentCount = handleCount(status.comments_count, title: "评论")
        unlikeCount = handleCount(status.attitudes_count, title: "赞")
        //  处理转发微博
        handleRetweetContent(status)
        //  处理来源数据
        handleSource(status.source ?? "")
        handleMbrankImage(status.user?.mbrank ?? 0)
        handleVerifiedTypeImage(status.user?.verified_type ?? -1)
    }
    //根据时间判断
    var timeContent: String? {
        //判断微博时间是否为空
        guard let createDateStr = status?.created_at else {
            return nil
        }
        let creatDate = NSDate.sinaDate(createDateStr)
        
        return creatDate.sinaDateString
    }
        
//    func isThisYear(createDate: NSDate) -> Bool {
//        let dt = NSDateFormatter()
//        //指定格式化方式
//        dt.dateFormat = "yyyy"
//        //指定本地化信息
//        dt.locale = NSLocale(localeIdentifier: "en_US")
//        //获取发微博时间的年份
//        let creatDateYear = dt.stringFromDate(createDate)
//        //获取当前时间的年份
//        let currentDateYear = dt.stringFromDate(NSDate())
//        //对比年份是否相同
//        return creatDateYear == currentDateYear
//        
//    }

//        //创建格式化对象
//        let dt = NSDateFormatter()
//        //格式化日期
//        dt.dateFormat = "EEE MMM dd HH:mm:ss z yyyy"
//        //指定本地化信息
//        dt.locale = NSLocale(localeIdentifier: "en_US")
//        //通过格式化方式转成时间对象
//        //当前时间加上多少秒
//        // NSDate().dateByAddingTimeInterval(-3600-1000)
//        let createDate = dt.dateFromString(createDateStr)!
        //把时间转化为字符串对象
//        if isThisYear(createDate) {
//            //是今年
//            //日历对象
//            let calendar = NSCalendar.currentCalendar()
//            if calendar.isDateInToday(createDate) {
//                //是今天
//                // createDate.timeIntervalSinceDate(<#T##anotherDate: NSDate##NSDate#>)
//                //获取发微博时间距离当前时间差多少秒
//                let timeInterVal = abs(createDate.timeIntervalSinceNow)
//                if timeInterVal < 60 {
//                    
//                    return "刚刚"
//                    
//                } else if timeInterVal < 3600 {
//                    
//                    let result = timeInterVal / 60
//                    
//                    return "\(Int(result))分钟前"
//                    
//                } else {
//                    
//                    let result = timeInterVal / 3600
//                    
//                    return "\(Int(result))小时前"
//                    
//                }
//            } else if calendar.isDateInYesterday(createDate) {
//                //昨天
//                dt.dateFormat = "昨天 HH:mm"
//            } else {
//                //其他
//                dt.dateFormat = "MM-dd HH:mm"
//            }
//        }else {
//            //非今年
//            dt.dateFormat = "yyyy-MM-dd HH:mm"
//            
//        }
//        
//        return dt.stringFromDate(createDate)
   // }
    
    //处理微博数据里面的逻辑
    //根据时间判断是否是今年
//    private func isThisYear(createDate: NSDate) -> Bool {
//        let dt = NSDateFormatter()
//        //指定格式化方式
//        dt.dateFormat = "yyyy"
//        //指定本地化信息
//        dt.locale = NSLocale(localeIdentifier: "en_US")
//        //获取发微博时间的年份
//        let creatDateYear = dt.stringFromDate(createDate)
//        //获取当前时间的年份
//        let currentDateYear = dt.stringFromDate(NSDate())
//        //对比年份是否相同
//        return creatDateYear == currentDateYear
//        
//    }
    
    //处理认证类型等级图片
    private func handleVerifiedTypeImage(verifidType: Int) {
        switch verifidType {
        case 0: verifiedTypeImage = UIImage(named: "avatar_vip")
        case 2,3,5: verifiedTypeImage = UIImage(named: "avatar_enterprise_vip")
        case 220: verifiedTypeImage = UIImage(named: "avatar_grassroot")
        default: verifiedTypeImage = nil
            
        }
        
    }
    //处理会员等级图片
    private func handleMbrankImage(mbrank: Int) {
        if mbrank >= 0 && mbrank <= 6 {
            
            mbrankImage = UIImage(named: "common_icon_membership_level\(mbrank)")
        }
    }
    //  处理微博数据里面的逻辑...
    //  处理来源数据
    private func handleSource(source: String) {
        //  判断字符串中是否包含这样关键字符串
        if source.containsString("\">") && source.containsString("</") {
            //  开始光标位置
            let startIndex = source.rangeOfString("\">")!.endIndex
            //  结束光标位置
            let endIndex = source.rangeOfString("</")!.startIndex
            //  获取指定范围的字符串
            let result = source.substringWithRange(startIndex..<endIndex)
            //  设置给来源数据
            sourceContent = "来自: " + result
        }
    }
    //  处理转发微博内容的拼接逻辑
    private func handleRetweetContent(status: StatusModel) {
        //  判断是否有转发微博内容
        if status.retweeted_status != nil {
            //  取到内容
            //  取到转发的用户昵称
            guard let text = status.retweeted_status?.text, let name = status.retweeted_status?.user?.screen_name else {
                return
            }
            
            //  拼接转发微博内容
            let result = "@\(name): \(text)"
            
            retweetContent = result
        }
        
        
        
    }
    
    private func handleCount(count: Int ,title: String) -> String {
        
        if count > 0 {
            if count >= 10000 {
                
                let result = CGFloat(count/1000)/10
                
                var resultStr = "\(result)万"
                if resultStr.containsString(".0") {
                    
                    resultStr = resultStr.stringByReplacingOccurrencesOfString(".0", withString: "")
                    
                }
                return resultStr
                
            }else {
                
                
                return "\(count)"
                
            }
            
        }else {
            return title
        }
    }
}


