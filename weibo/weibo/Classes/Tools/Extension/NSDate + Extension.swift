//
//  NSDate + Extension.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/15.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

extension NSDate {
    class func sinaDate(createDateStr: String) -> NSDate {
        let dt = NSDateFormatter()
        //格式化日期
        dt.dateFormat = "EEE MMM dd HH:mm:ss z yyyy"
        //指定本地化信息
        dt.locale = NSLocale(localeIdentifier: "en_US")
        //通过格式化方式转成时间对象
        //当前时间加上多少秒
        // NSDate().dateByAddingTimeInterval(-3600-1000)
        let createDate = dt.dateFromString(createDateStr)!
        return createDate
    }
    var sinaDateString: String {
        let dt = NSDateFormatter()
        dt.locale = NSLocale(localeIdentifier: "en_US")
        
        if isThisYear(self) {
            //是今年
            //日历对象
            let calendar = NSCalendar.currentCalendar()
            if calendar.isDateInToday(self) {
                //是今天
                // createDate.timeIntervalSinceDate(<#T##anotherDate: NSDate##NSDate#>)
                //获取发微博时间距离当前时间差多少秒
                let timeInterVal = abs(self.timeIntervalSinceNow)
                if timeInterVal < 60 {
                    
                    return "刚刚"
                    
                } else if timeInterVal < 3600 {
                    
                    let result = timeInterVal / 60
                    
                    return "\(Int(result))分钟前"
                    
                } else {
                    
                    let result = timeInterVal / 3600
                    
                    return "\(Int(result))小时前"
                    
                }
            } else if calendar.isDateInYesterday(self) {
                //昨天
                dt.dateFormat = "昨天 HH:mm"
            } else {
                //其他
                dt.dateFormat = "MM-dd HH:mm"
            }
        }else {
            //非今年
            dt.dateFormat = "yyyy-MM-dd HH:mm"
            
        }
        
        return dt.stringFromDate(self)

    
    
    }
    
  private func isThisYear(createDate: NSDate) -> Bool {
        let dt = NSDateFormatter()
        //指定格式化方式
        dt.dateFormat = "yyyy"
        //指定本地化信息
        dt.locale = NSLocale(localeIdentifier: "en_US")
        //获取发微博时间的年份
        let creatDateYear = dt.stringFromDate(createDate)
        //获取当前时间的年份
        let currentDateYear = dt.stringFromDate(NSDate())
        //对比年份是否相同
        return creatDateYear == currentDateYear
        
    }




}














