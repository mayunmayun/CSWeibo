//
//  UILabel + Extension.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

//便利构造函数
extension UILabel {
    
    convenience init(fontSize: CGFloat, textColor: UIColor) {
        self.init()
       self.font = UIFont.systemFontOfSize(fontSize)
        
        self.textColor = textColor
        
        
    }
    


}







