//
//  UIBarButtonItem + Extension.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/9.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
   

    convenience init(title: String, target: AnyObject?, action:Selector) {
    self.init()
        let button = UIButton()
        //添加点击事件
        button.addTarget(target, action: action, forControlEvents: .TouchUpInside)
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        button.setTitleColor(UIColor.orangeColor(), forState: .Highlighted)
        button.titleLabel?.font = UIFont.systemFontOfSize(14)
        button.sizeToFit()
        //设置自定义视图
        customView = button
    }



}


















