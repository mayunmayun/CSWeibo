//
//  Common.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/11.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
//切换根视图控制器的通知名
let switchRootVController = "switchRootVController"
//  当前屏幕的宽度
let ScreenWidth = UIScreen.mainScreen().bounds.size.width
//  当前屏幕的高度
let ScreenHeight = UIScreen.mainScreen().bounds.size.height

//  通过随机数返回颜色对象
func RandomColor() -> UIColor {
    
    //  色值得取值范围0-255
    let red = random() % 256
    let green = random() % 256
    let blue = random() % 256
    
    
    // red ,green ,blue, alpha 他们的取值范围0-1之间
    return UIColor(red: CGFloat(red) / 255  , green: CGFloat(green) / 255 , blue: CGFloat(blue) / 255 , alpha: 1)
    
}











