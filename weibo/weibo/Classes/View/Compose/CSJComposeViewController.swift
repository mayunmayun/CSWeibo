//
//  CSJComposeViewController.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/16.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SVProgressHUD
class CSJComposeViewController: UIViewController {
    //懒加载控件
    //右边按钮
    private lazy var rightButton: UIButton = {
        let button = UIButton()
        button.setTitle("发送", forState: .Normal)
    button.titleLabel?.font = UIFont.systemFontOfSize(15)
        //设置不同状态文字颜色
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.setTitleColor(UIColor.grayColor(), forState: .Disabled)
        //设置不同状态的背景图片
        button.setBackgroundImage(UIImage(named: "common_button_orange"), forState: .Normal)
        button.setBackgroundImage(UIImage(named: "common_button_orange_highlighted"), forState: .Highlighted)
        button.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: .Disabled)
        button.size = CGSize(width: 45, height: 30)
        
        return button
    }()
    //标题视图
    private lazy var titleLabel: UILabel = {
        let title = UILabel(fontSize: 17, textColor: UIColor.darkGrayColor())
        
        if let name = CSJUserAccountViewModel.shardUserAccount.userAccount?.name {
        
            let res = "发微博\n\(name)"
            
            let range = (res as NSString).rangeOfString(name)
            
            let attributedStr = NSMutableAttributedString(string: res)
            
            attributedStr.addAttributes([NSForegroundColorAttributeName:UIColor.lightGrayColor(),NSFontAttributeName: UIFont.systemFontOfSize(14)], range: range)
            title.attributedText = attributedStr
                        //添加附文本
        }else {
        
            title.text = "发微博"
        }
        
        title.numberOfLines = 0
        title.textAlignment = .Center
        title.sizeToFit()

        return title
    }()
    //发微博视图
    private lazy var textView: CSJComposeTextView = {
        let view = CSJComposeTextView()
        
        
    
        return view
    }()
    
    //设置导航栏视图
    private func setupNavUI() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "取消", target: self, action: "cancelButton")
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        //设置右边按钮不可用
        navigationItem.rightBarButtonItem?.enabled = false
        
        navigationItem.titleView = titleLabel
    
    }
    
    @objc private func cancelButton() {
    
    dismissViewControllerAnimated(true, completion: nil)
    
    
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blueColor()
        setupNavUI()
    }
    
   

}
