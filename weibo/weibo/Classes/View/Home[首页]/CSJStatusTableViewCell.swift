//
//  CSJStatusTableViewCell.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//
import UIKit
import SnapKit
let StatusCellMargin: CGFloat = 10
class CSJStatusTableViewCell: UITableViewCell {
    //记录约束
    var toolBarTopConstant: Constraint?
    
    var statusModel: CSJStatusModel? {
        
        didSet{
            
            OriginView.statusViewModel = statusModel
            toolBar.statusViewModel = statusModel
            
            //卸载上一次的约束
            toolBarTopConstant?.uninstall()
            
            //判断是否有转发微博对象
            if statusModel?.status?.retweeted_status != nil {
                retweetView.hidden = false
                
              toolBar.snp_updateConstraints(closure: {   (make) -> Void in
                     self.toolBarTopConstant =  make.top.equalTo(retweetView.snp_bottom).constraint
                })
                
              retweetView.statusViewModel = statusModel
            }else {
            
            retweetView.hidden = true
                
            toolBar.snp_makeConstraints(closure: { (make) -> Void in
                self.toolBarTopConstant =  make.top.equalTo(OriginView.snp_bottom).constraint
            })
            }
            
            
        }
    }
    
    
    //懒加载控件
    //原创微博视图
    private lazy var OriginView: CSJStatusOrginView = {
        let view = CSJStatusOrginView()
       // view.backgroundColor = RandomColor()
        return view
    }()
    //toolBar
    private lazy var toolBar: CSJStatusToolBarView = CSJStatusToolBarView()
    //转发微博视图
    private lazy var retweetView: CSJStatusRetweetView = {
        let retweet = CSJStatusRetweetView()
        
        return retweet
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupUI() {
        contentView.addSubview(OriginView)
        contentView.addSubview(toolBar)
        contentView.addSubview(retweetView)
        //  修改contentView的背景色
        contentView.backgroundColor = UIColor(white: 0.95, alpha: 1) 
        //设置约束
        OriginView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(contentView).offset(8) 
            make.leading.equalTo(contentView)
            make.trailing.equalTo(contentView)
        }
        toolBar.snp_makeConstraints { (make) -> Void in
         self.toolBarTopConstant = make.top.equalTo(retweetView.snp_bottom).constraint
            make.leading.equalTo(contentView)
            make.trailing.equalTo(contentView)
            make.height.equalTo(35)
            
        }
        contentView.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(toolBar)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.top.equalTo(self)
        }
        retweetView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(OriginView.snp_bottom)
            make.leading.equalTo(OriginView)
            make.trailing.equalTo(OriginView)
            
        }
        
    }
    
}
