//
//  CSJStatusRetweetView.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SnapKit
//转发微博视图

class CSJStatusRetweetView: UIView {
    //接收约束
    var statusSetweetBotton: Constraint?
    
    
    //var a: StatusModel?//var a = StatusModel()
    
    
    //***使用viewModel绑定数据/
    var statusViewModel: CSJStatusModel? {
        didSet {
            
            statusSetweetBotton?.uninstall()
            
            contentLabel.text = statusViewModel?.retweetContent
            
            if let pic_url = statusViewModel?.status?.retweeted_status?.pic_urls where pic_url.count > 0 {
                //显示配图
                pictureView.hidden = false
                 
                self.snp_makeConstraints(closure: { (make) -> Void in
                    self.statusSetweetBotton = make.bottom.equalTo(pictureView).offset(StatusCellMargin).constraint
                    
                })
                
                pictureView.pic_url = pic_url
                
            } else {
                
                pictureView.hidden = true
                self.snp_makeConstraints(closure: { (make) -> Void in
                    self.statusSetweetBotton = make.bottom.equalTo(contentLabel).offset(StatusCellMargin).constraint
                })
            }
        }
    }
    //懒加载控件
    private lazy var contentLabel: UILabel = {
        let label = UILabel(fontSize: 14, textColor: UIColor.grayColor())
        
        label.numberOfLines = 0
        
        return label
    }()
    
    private lazy var pictureView: CSJStatusPictureView = {
        let picture = CSJStatusPictureView()
        picture.backgroundColor = self.backgroundColor
        return picture
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        backgroundColor = UIColor(white: 0.95, alpha: 1)
        //添加控件
        addSubview(contentLabel)
        addSubview(pictureView)
        //设置约束
        contentLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(StatusCellMargin)
            make.leading.equalTo(self).offset(StatusCellMargin)
            make.trailing.equalTo(self).offset(-StatusCellMargin)
            
        }
        pictureView.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(contentLabel)
            make.top.equalTo(contentLabel.snp_bottom).offset(StatusCellMargin)
        }
        //转发微博的底部约束
        self.snp_makeConstraints { (make) -> Void in
            self.statusSetweetBotton = make.bottom.equalTo(pictureView).offset(StatusCellMargin).constraint
            
        }
        
    }
    
    
    
}
