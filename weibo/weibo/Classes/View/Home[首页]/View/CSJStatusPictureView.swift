//
//  CSJStatusPictureView.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/13.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SDWebImage
//  重用标记
private let StatusPictureViewCellIdentifier = "StatusPictureViewCellIdentifier"
//  每项之间间距
private let itemMargin: CGFloat = 5
//  每项的宽度
private let itemWidth = (ScreenWidth - 2 * StatusCellMargin - 2 * itemMargin) / 3

class CSJStatusPictureView: UICollectionView {
    var pic_url: [CSJStatusPictureInfo]? {
        didSet {
            messageLabel.text = "\(pic_url?.count ?? 0)"
            //根据视图的个数算大小
            let size = calcSizeWithCount(pic_url?.count ?? 0)
            self.snp_updateConstraints { (make) -> Void in
                make.size.equalTo(size)
            }
            self.reloadData()
        }
        
    }
    //显示配图个数
    private lazy var messageLabel: UILabel = UILabel(fontSize: 20, textColor: UIColor.whiteColor())
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let flowLayout = UICollectionViewFlowLayout()
        
        //设置大小
        flowLayout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        //水平间距
        flowLayout.minimumInteritemSpacing = itemMargin
        //垂直间距
        flowLayout.minimumLineSpacing = itemMargin
        super.init(frame: frame, collectionViewLayout: flowLayout)
        setupUI()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        registerClass (CSJtatusPictureViewCell.self, forCellWithReuseIdentifier: StatusPictureViewCellIdentifier)
        //设置数据源代理
        dataSource = self
        addSubview(messageLabel)
        messageLabel.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self)
        }
    }
    
    private func calcSizeWithCount(count: Int) -> CGSize {
        //列数
        let cols = count > 3 ? 3 : count
        
        let rows = (count - 1) / 3 + 1
        
        //宽度
        let currentWith = itemWidth * CGFloat(cols) + CGFloat(cols) + CGFloat(cols - 1) * itemMargin
        //高度
        let currentHeight = itemWidth * CGFloat(rows) + CGFloat(rows - 1) * itemMargin
        
        return CGSize(width: currentWith, height: currentHeight)
    }
}
extension CSJStatusPictureView: UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pic_url?.count ?? 0
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(StatusPictureViewCellIdentifier, forIndexPath: indexPath) as! CSJtatusPictureViewCell
        //设置模型数据
        cell.picInfo = pic_url![indexPath.item]
        
        
        return cell
    }
}
class CSJtatusPictureViewCell: UICollectionViewCell {
    var picInfo: CSJStatusPictureInfo? {
        didSet {
            if let url = picInfo?.thumbnail_pic {
                
                imageView.sd_setImageWithURL(NSURL(string: url), placeholderImage: UIImage(named: "timeline_image_placeholder"))
                gifImageView.hidden = !url.hasSuffix(".gif")
            }
        }
    }
    private lazy var imageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "timeline_image_placeholder"))
        view.contentMode = UIViewContentMode.ScaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    private lazy var gifImageView: UIImageView = UIImageView(image: UIImage(named: "timeline_image_gif"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func setupUI() {
        //添加控件
        contentView.addSubview(imageView)
        contentView.addSubview(gifImageView)
        //设置约束
        imageView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).offset(UIEdgeInsetsZero)
        }
        gifImageView.snp_makeConstraints { (make) -> Void in
            make.trailing.equalTo(imageView)
            make.bottom.equalTo(imageView)
        }
    }
}








