//
//  CSJStatusToolBarView.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
//微博tooBar
class CSJStatusToolBarView: UIView {
    

    //  转发按钮
    var retweetButton: UIButton?
    //  评论按钮
    var commentButton: UIButton?
    //  赞按钮
    var unlikeButton: UIButton?
    
    var statusViewModel: CSJStatusModel? {
        didSet {
            retweetButton?.setTitle(statusViewModel?.retweetCount, forState: .Normal)
            commentButton?.setTitle(statusViewModel?.commentCount, forState: .Normal)
            unlikeButton?.setTitle(statusViewModel?.unlikeCount, forState: .Normal)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  添加控件设置约束
    private func setupUI() {
        
        //  添加按钮
        retweetButton = addChildButton("timeline_icon_retweet", title: "转发")
        commentButton = addChildButton("timeline_icon_comment", title: "评论")
        unlikeButton = addChildButton("timeline_icon_unlike", title: "赞")
        //  添加竖线
        let firstLineView = addChildLineView()
        let secondLineView = addChildLineView()
        
        retweetButton!.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(self)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            make.width.equalTo(commentButton!)
        }
        
        commentButton!.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(retweetButton!.snp_trailing)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            make.width.equalTo(unlikeButton!)
        }
        
        unlikeButton!.snp_makeConstraints { (make) -> Void in
            make.trailing.equalTo(self)
            make.top.equalTo(self)
            make.bottom.equalTo(self)
            make.leading.equalTo(commentButton!.snp_trailing)
        }
        
        firstLineView.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(retweetButton!.snp_trailing)
            make.centerY.equalTo(self)
        }
        
        secondLineView.snp_makeConstraints { (make) -> Void in
            make.centerY.equalTo(self)
            make.centerX.equalTo(commentButton!.snp_trailing)
        }
        
    }
    //  添加子按钮的通用方法
    private func addChildButton(imageName: String, title: String) -> UIButton {
        let button = UIButton()
        //  设置图片
        button.setImage(UIImage(named: imageName), forState: .Normal)
        //  设置背景图片
        button.setBackgroundImage(UIImage(named: "timeline_card_bottom_background"), forState: .Normal)
        //  设置文字与颜色
        button.setTitle(title, forState: .Normal)
        button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        
        //  设置字体
        button.titleLabel?.font = UIFont.systemFontOfSize(15)
        
        addSubview(button)
        
        return button
    }
    
    //  添加子竖线的通用方法
    private func addChildLineView() -> UIImageView {
        let imageView = UIImageView(image: UIImage(named: "timeline_card_bottom_line"))
        addSubview(imageView)
        return imageView
    }
    
    
    

}
