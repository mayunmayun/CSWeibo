//
//  CSJStatusOrginView.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
    //原创微博视图
class CSJStatusOrginView: UIView {
    
    var originViewConstraint: Constraint?
    
    //设置控件数据
    var statusViewModel: CSJStatusModel? {
        didSet {
            if let profile = statusViewModel?.status?.user?.profile_image_url {
            headerImageView.sd_setImageWithURL(NSURL(string: profile), placeholderImage: UIImage(named: "avatar_default_big"))
            
            }
            //昵称
        nameLabel.text = statusViewModel?.status?.user?.screen_name
        //微博数据
            contentLabel.text = statusViewModel?.status?.text
            //来源
            sourceLabel.text = statusViewModel?.status?.source
            
            mbrankImageview.image = statusViewModel?.mbrankImage
            //  设置认证类型等级图片
            vipTypeImageView.image = statusViewModel?.verifiedTypeImage
            
            timeLabel.text = statusViewModel?.timeContent
            
            //  卸载上次约束
            originViewConstraint?.uninstall()
            //  判断原创微博的配图个数是否大于0
            if let picUrls = statusViewModel?.status?.pic_urls where picUrls.count > 0 {
                //  有配图并且个数大于0
                //  显示配图
                pictureView.hidden = false
                //  绑定原创微博数据源
                pictureView.pic_url = picUrls
                
                //  更新约束
                self.snp_updateConstraints(closure: { (make) -> Void in
                    self.originViewConstraint = make.bottom.equalTo(pictureView).offset(StatusCellMargin).constraint
                })
                
                
            } else {
                //  隐藏配图
                pictureView.hidden = true
                self.snp_updateConstraints(closure: { (make) -> Void in
                    self.originViewConstraint = make.bottom.equalTo(contentLabel).offset(StatusCellMargin).constraint
                })
            }
        }
    }
    
    //懒加载控件
    //头像
    private lazy var headerImageView: UIImageView = UIImageView(image: UIImage(named: "avatar_default_big"))
    //认证等级图片
    private lazy var vipTypeImageView: UIImageView = UIImageView(image: UIImage(named: "avatar_vip"))
    //昵称
    private lazy var nameLabel: UILabel = UILabel(fontSize: 15, textColor: UIColor.darkGrayColor())
    //会员等级
    private lazy var mbrankImageview: UIImageView = UIImageView(image: UIImage(named: "common_icon_membership"))
    //时间
    private lazy var timeLabel: UILabel = UILabel(fontSize: 12, textColor: UIColor.orangeColor())
    //来源
    private lazy var sourceLabel: UILabel = UILabel(fontSize: 12, textColor: UIColor.lightGrayColor())
    //微博内容
    private lazy var contentLabel: UILabel = {
        let label = UILabel(fontSize: 14, textColor:
            UIColor.darkGrayColor())
        label.numberOfLines = 0
        return label
    }()
    

    //  配图
    private lazy var pictureView: CSJStatusPictureView = {
        let view = CSJStatusPictureView()
        view.backgroundColor = self.backgroundColor
        return view
        
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //添加控件约束
    private func setupUI() {
        backgroundColor = UIColor.whiteColor()
        addSubview(headerImageView)
        addSubview(vipTypeImageView)
        addSubview(nameLabel)
        addSubview(mbrankImageview)
        addSubview(timeLabel)
        addSubview(sourceLabel)
        addSubview(contentLabel)
        addSubview(pictureView)
       // timeLabel.text = "刚刚"
        
        //设置约束
        headerImageView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(StatusCellMargin)
            make.leading.equalTo(self).offset(StatusCellMargin)
            make.size.equalTo(CGSize(width: 35, height: 35))
        }
        vipTypeImageView.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(headerImageView.snp_trailing)
            make.centerY.equalTo(headerImageView.snp_bottom)
            
        }
        nameLabel.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(headerImageView.snp_trailing).offset(StatusCellMargin)
            make.top.equalTo(headerImageView)
            
        }
        mbrankImageview.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(nameLabel.snp_trailing).offset(StatusCellMargin)
            make.top.equalTo(nameLabel)
        }
        timeLabel.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(nameLabel)
            make.bottom.equalTo(headerImageView)
        }
        sourceLabel.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(headerImageView)
            make.leading.equalTo(timeLabel.snp_trailing).offset(StatusCellMargin)
        }
        contentLabel.snp_makeConstraints { (make) -> Void in
            make.leading.equalTo(headerImageView)
            make.top.equalTo(headerImageView.snp_bottom).offset(StatusCellMargin)
            make.trailing.equalTo(self).offset(-StatusCellMargin)
        }
        
        
        pictureView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(contentLabel.snp_bottom).offset(StatusCellMargin)
            make.leading.equalTo(contentLabel)
        }
        //关键约束
        self.snp_makeConstraints { (make) -> Void in
        self.originViewConstraint = make.bottom.equalTo(pictureView).offset(StatusCellMargin).constraint
        }
        
        
        
        
    }
    
    
}
