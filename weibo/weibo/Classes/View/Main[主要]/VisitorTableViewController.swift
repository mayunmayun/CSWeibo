//
//  VisitorTableViewController.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/8.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

class VisitorTableViewController: UITableViewController {
    //是否登录的标记
    var isLogin: Bool = CSJUserAccountViewModel.shardUserAccount.userAccount?.access_token != nil
      var loainView: CSJLoainView?
    override func loadView() {
        if isLogin {
         super.loadView()
            
        } else {
            loainView = CSJLoainView()
            
            loainView?.loginClose = { [weak self] in
                
            self?.reqeustOAuthVC()
            }
            view = loainView
            setNVup()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

   private func setNVup() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", target: self, action: "registButton")
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", target: self, action: "loginButton")
    
    }
    
    @objc private func registButton() {
    reqeustOAuthVC()
    
    }
    
    @objc private func loginButton() {
    reqeustOAuthVC()
        
    }

    //登录第三方
    private func reqeustOAuthVC() {
    let oAuch = CSOAuthViewController()
        let nav = UINavigationController(rootViewController: oAuch)
    presentViewController(nav, animated: true, completion: nil)
    
    }
    
    
    
   

}
