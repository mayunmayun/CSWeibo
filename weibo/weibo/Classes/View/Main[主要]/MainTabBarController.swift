//
//  MainTabBarController.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/7.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //设置自定义tabBar
        let cstabBar = CSJTabBar()
        
        cstabBar.csdelegate = self
        
        cstabBar.composeButtonClose = { [weak self] in
            
         self?.pushComposeVC()
            
            print("我是闭包调用过来的")
        }
        
        setValue(cstabBar, forKey: "tabBar")
        
        addChildViewController(HomeTableViewController(), title: "首页", image: "tabbar_home")
        addChildViewController(MessageTableViewController(), title: "消息", image: "tabbar_message_center")
        addChildViewController(DiscoverTableViewController(), title: "发现", image: "tabbar_discover")
        addChildViewController(MeTableViewController(), title: "我", image: "tabbar_profile")
    }
    //进入发布微博界面
    private func pushComposeVC() {
        let compose = CSJComposeViewController()
        
        let vc = UINavigationController(rootViewController: compose)
        
        presentViewController(vc, animated: true, completion: nil)
        
    
    
    }
    
    
   func addChildViewController(childView:UIViewController, title: String, image: String) {
        childView.title = title
        childView.tabBarItem.image = UIImage(named: image)
        childView.tabBarItem.selectedImage = UIImage(named: image + "_highlighted")?.imageWithRenderingMode(.AlwaysOriginal)
        
        //设置文字颜色
        childView.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.orangeColor()], forState: .Selected)
        //设置文字大小
        childView.tabBarItem.setTitleTextAttributes([NSFontAttributeName: UIFont.systemFontOfSize(14)], forState: .Normal)
        
        
        //包装导航控制器
        let childNv = UINavigationController(rootViewController: childView)
        
        
        addChildViewController(childNv)
        
    }
   
}
extension MainTabBarController: CSJTabBarDelegate {

    func didSelectedCSJTabBar() {
        print("我是代理")
    }


}











