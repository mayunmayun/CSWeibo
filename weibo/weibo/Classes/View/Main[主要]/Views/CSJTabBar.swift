//
//  CSJTabBar.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/8.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

protocol CSJTabBarDelegate :NSObjectProtocol {

    func didSelectedCSJTabBar()

}

class CSJTabBar: UITabBar {
    
    var composeButtonClose: (()->())?
    
    //定义代理对象
    
   weak var csdelegate: CSJTabBarDelegate?
    
    //  MARK: -- 懒加载
    private lazy var composeButton: UIButton = {
        let button = UIButton()
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: .Normal)
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: .Highlighted)
        button.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: .Normal)
        button.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: .Highlighted)
        
        button.addTarget(self, action: "composeButtonAction", forControlEvents: .TouchUpInside)
        
        button.sizeToFit()
        
        
        return button
        
    }()
    //MARK: -- 加号按钮点击
  @objc private func composeButtonAction() {
    
    composeButtonClose?()
    //使用代理对象调用代理方法
    csdelegate?.didSelectedCSJTabBar()
  

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        
        setupUI()
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)//只有写这个方法才能使用xib/sb
        setupUI()
    }
    
    private func setupUI() {
    
    addSubview(composeButton)
    
        
        
    }
    
   //布局子控件
    
    override func layoutSubviews() {
       
        super.layoutSubviews()
        
        composeButton.centerX = width * 0.5
        
        composeButton.centerY = height * 0.5
        
        var index = 0
        
        let buttonWidth = width / 5
        
        for value in subviews {
        
            if value.isKindOfClass(NSClassFromString("UITabBarButton")!) {
            value.width = buttonWidth
                
            value.height = height
            
            value.x = CGFloat(index) * buttonWidth
                
            index++
               
                if index == 2 {
                
                    index++
                
                }
                
            
            }
        
        }
        
        
    }
    

}












