//
//  CSJLoainView.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/8.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SnapKit
class CSJLoainView: UIView {
    
    var loginClose: (()->())?
    
    //MARK: - 懒加载控件
    //旋转图片
    private lazy var roteImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    //罩层
    private lazy var maskImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    //主页图片
    private lazy var iconImageView: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    //消息设置
    private lazy var messageLable: UILabel = {
    let lable = UILabel()
        
    lable.text = "关注一些人，回这里看看有什么惊喜关注一些人，回这里看看有什么惊喜"
       lable.textColor = UIColor.darkGrayColor()
        
        lable.textAlignment = NSTextAlignment.Center
        
        lable.font = UIFont.systemFontOfSize(14)
        
        //多行显示
        lable.numberOfLines = 0
        
    return lable
    }()
    //注册
    private lazy var registerButton: UIButton = {
    let button = UIButton()
        
        button.addTarget(self, action: "resgistButton", forControlEvents: .TouchUpInside)
        
        button.setBackgroundImage(UIImage(named: "common_button_white_disable") , forState: .Normal)
        button.setTitle("注册", forState: .Normal)
    
        button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        
        button.setTitleColor(UIColor.orangeColor(), forState: .Highlighted)
        button.titleLabel?.font = UIFont.systemFontOfSize(14)
       
    return button
    }()
    
    //登录
    private lazy var loginButton: UIButton = {
        
        let button = UIButton()
       
         button.addTarget(self, action: "loginbutton", forControlEvents: .TouchUpInside)
        
        button.setBackgroundImage(UIImage(named: "common_button_white_disable") , forState: .Normal)
        button.setTitle("登录", forState: .Normal)
        
        button.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
        
        button.setTitleColor(UIColor.orangeColor(), forState: .Highlighted)
        button.titleLabel?.font = UIFont.systemFontOfSize(14)
        
        return button
    }()
    
    
    @objc private func loginbutton() {
        
        loginClose?()
        
        
    }
    
    @objc private func resgistButton() {
    
    loginClose?()
    
    
    }
    
    
    
    override init(frame: CGRect) {
      super.init(frame: frame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    //开启动画
    private func startAnimation() {
        //创建核心动画
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = 2 * M_PI
        animation.duration = 20
        animation.repeatCount = MAXFLOAT
        animation.removedOnCompletion = false
        //执行动画
        roteImageView.layer.addAnimation(animation, forKey: nil)
    }
    //修改访客视图信息
    func updateVistorViewInfo(message: String?,imgName: String?) {
        if let mas = message,imName = imgName {
        
            messageLable.text = mas
            
            iconImageView.image = UIImage(named: imName)
        
            roteImageView.hidden = true
        }else {
        
        roteImageView.hidden = false
            
            startAnimation()
        
        
        }
    
    }
    
    private func setupUI() {
        backgroundColor = UIColor(white: 237.0/255.0, alpha: 1)
        
    //添加控件
        addSubview(roteImageView)
        addSubview(maskImageView)
        addSubview(iconImageView)
        addSubview(messageLable)
        addSubview(registerButton)
        addSubview(loginButton)
        
     //设置约束
        roteImageView.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self)
        }
        maskImageView.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self)
        }
        iconImageView.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self)
        }
        messageLable.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(roteImageView.snp_bottom)
            make.centerX.equalTo(roteImageView)
            make.width.equalTo(224)
            
        }
        registerButton.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(messageLable.snp_bottom).offset(10)
            make.leading.equalTo(messageLable.snp_leading)
            make.width.equalTo(100)
            make.height.equalTo(35)
        }
        loginButton.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(messageLable.snp_bottom).offset(10)
            make.trailing.equalTo(messageLable.snp_trailing)
            make.width.equalTo(100)
            make.height.equalTo(35)
        }
    }
}


extension CSJLoainView {

    func text() {
    
        roteImageView.translatesAutoresizingMaskIntoConstraints=false
        addConstraint(NSLayoutConstraint(item: roteImageView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: roteImageView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0))
        
        

    
    
    }


}













