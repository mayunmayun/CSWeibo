//
//  CSJViewController.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/11.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit
class CSJViewController: UIViewController {
    //背景
    lazy var bJImage: UIImageView = UIImageView(image: UIImage(named: "ad_background"))
    //头像
    lazy var iconImg: UIImageView = {
        let ima = UIImageView(image: UIImage(named: "avatar_default_big"))
        ima.layer.cornerRadius = 50;
    //切掉多余图片
        ima.layer.masksToBounds = true
    return ima
    }()
    
    //欢迎信息
    
    lazy var titleLable: UILabel = {
    let title = UILabel()
    title.font = UIFont.systemFontOfSize(14)
        
    title.textColor = UIColor.darkGrayColor()
        
        if let avatar_large = CSJUserAccountViewModel.shardUserAccount.userAccount?.avatar_large {
        
        self.iconImg.sd_setImageWithURL(NSURL(string: avatar_large), placeholderImage: UIImage(named: "avatar_default_big"))
        }
        if let name = CSJUserAccountViewModel.shardUserAccount.userAccount?.name {
        
        title.text = "欢迎回来\(name)"
            
        
        }else {
        
        title.text = "欢迎回来"
        
        }
        
        return title
    }()
    
    override func loadView() {
        view = bJImage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    //页面已经显示出来执行动画
    override func viewDidAppear(animated: Bool) {
        startAnmation()
    }
    
    private func startAnmation() {
        titleLable.alpha = 0
        iconImg.snp_updateConstraints { (make) -> Void in
            make.top.equalTo(view).offset(100)
        }
    UIView.animateWithDuration(1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: { () -> Void in
        self.view.layoutIfNeeded()
        }) { (_) -> Void in
            UIView.animateWithDuration(1, animations: { () -> Void in
                self.titleLable.alpha = 1
                }, completion: { (_) -> Void in
                 //发送通知
                    NSNotificationCenter.defaultCenter().postNotificationName(switchRootVController, object: nil)
            })
        }
    }
    
    private func setupUI() {
        view.addSubview(iconImg)
        
        view.addSubview(titleLable)
        iconImg.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(view)
            make.top.equalTo(view).offset(200)
            make.size.equalTo(CGSize(width: 100, height: 100))
            
        }
    titleLable.snp_makeConstraints { (make) -> Void in
        make.top.equalTo(iconImg.snp_bottom).offset(10)
        make.centerX.equalTo(iconImg)
        }
    
    }

}
