//
//  UserModel.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    //  用户id
    var id: Int64 = 0
    //  用户昵称
    var screen_name: String?
    //  用户的头像
    var profile_image_url: String?
    //  认证类型等级 -1 没有认证 ，0 认证用户，2，3，5 企业认证 ， 220 达人
    var verified_type: Int = 0
    //  会员等级 1-6
    var mbrank: Int = 0
    
    //kvc构造函数
    init(dic: [String :AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dic)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
    
    
    
    
    
}
