//
//  CSJStatusPictureInfo.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/14.
//  Copyright © 2016年 cui. All rights reserved.
//
import UIKit
//配图模型
class CSJStatusPictureInfo: NSObject {
    //  配图地址
    var thumbnail_pic: String?
    init(dict: [String :AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
}
