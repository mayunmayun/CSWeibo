//
//  CSJUserAccount.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/10.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

//用户账号模型
class CSJUserAccount: NSObject, NSCoding {
    var access_token: String?
    var expires_in: Int = 0
    var remind_in: NSTimeInterval = 0 {
        didSet {
        
        expiresDate = NSDate().dateByAddingTimeInterval(remind_in)
        
        }
    }
    //过期时间
    var expiresDate: NSDate?
    
    var uid: Int64 = 0
    var avatar_large: String?
    var name: String?
    
    
    
    init(dict: [String: AnyObject]) {
        
        super.init()
        
        setValuesForKeysWithDictionary(dict)
        
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
    
   override var description: String {
    
        let keys = ["access_token","expires_in","remind_in","uid","avatar_large","name"]
        
        return  dictionaryWithValuesForKeys(keys).description
    
    }
   //归档
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeObject(expiresDate, forKey: "expiresDate")
        aCoder.encodeInt64(uid, forKey: "uid")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        aCoder.encodeObject(name, forKey: "name")
        
    }
   //解档
    required init?(coder aDecoder: NSCoder) {
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        expiresDate = aDecoder.decodeObjectForKey("expiresDate") as? NSDate
        uid = aDecoder.decodeInt64ForKey("uid")
        
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
        
        name = aDecoder.decodeObjectForKey("name") as? String
    }
    
    //保存用户数据到指定shahe
    func saveUserAccount() {
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("cui.123")
        //执行归档
        NSKeyedArchiver.archiveRootObject(self, toFile: path)
    }
    //类函数
    
    class func readUserAccount() ->CSJUserAccount? {
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("cui.123")
        print(path)
        //执行解档
      return NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? CSJUserAccount
        
    }
    
    
    
}




