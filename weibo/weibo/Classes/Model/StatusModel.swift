//
//  StatusModel.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/12.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit
//微博模型
class StatusModel: NSObject {
    //  发微博时间
    var created_at: String?
    //  微博id
    var id: Int64 = 0
    //  微博内容
    var text: String?
    //  来源
    var source: String?
    //  关注用户的模型
    var user: UserModel?
    //  转发数
    var reposts_count: Int = 0
    //  评论数
    var comments_count:	Int = 0
    //	表态数
    var attitudes_count: Int = 0
    //  转发微博对象
    var retweeted_status: StatusModel?
    //匹配对象
    var pic_urls: [CSJStatusPictureInfo]?

//kvc构造函数
    init(dic: [String :AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dic)
    }
    
    override func setValue(value: AnyObject?, forKey key: String) {
        
        if key == "user" {
        
            guard let dic = value as? [String :AnyObject] else {
            
            return
            }
        user = UserModel(dic: dic)
        }else if key == "retweeted_status" {
        
            guard let dict = value as? [String :AnyObject] else {
            return
            }
            
            
            
        retweeted_status = StatusModel(dic: dict)
        } else if key == "pic_urls" {
        
            guard let dict = value as? [[String :AnyObject]] else {
            return
            }
            var temArray = [CSJStatusPictureInfo]()
            for result in dict {
            
                let picture = CSJStatusPictureInfo(dict: result)
            temArray.append(picture)
            }
        pic_urls = temArray
            
        } else {
            super.setValue(value, forKey: key)
    }
}
    
    
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
