//
//  AppDelegate.swift
//  weibo
//
//  Created by 崔顺建 on 16/8/7.
//  Copyright © 2016年 cui. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        //注册监听的通知
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "switchRootVc:", name: switchRootVController, object: nil)
        
//       let read = CSJUserAccount.readUserAccount()
//        print("############\(read)")
//        
        //设置全局颜色
        UITabBar.appearance().tintColor = UIColor.orangeColor()
        
        //创建window
        window = UIWindow(frame:  UIScreen.mainScreen().bounds)
        
        let root = MainTabBarController()
        //设置跟控制器
        window?.rootViewController = root
        
        //设置并显示
        window?.makeKeyAndVisible()
        

        
                return true
    }
    //监听通知的方法
    @objc private func switchRootVc(noty: NSNotification) {
        let obj = noty.object
        if obj is CSOAuthViewController {
            let root = CSJViewController()
            //设置跟控制器
            window?.rootViewController = root
        
        
        }else {
        
            let root = MainTabBarController()
            //设置跟控制器
            window?.rootViewController = root
        
        }
        
    
    }
    
    deinit {
    
        NSNotificationCenter.defaultCenter().removeObserver(self)
    
    }
    
    
}

